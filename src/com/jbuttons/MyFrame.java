package com.jbuttons;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class MyFrame extends JFrame implements ActionListener {

	JButton buttonPlay;
	JButton buttonStop;
	ImageIcon icon;
	File file;
	Clip clip;

	public MyFrame() {

		buttonPlay = new JButton();
		buttonPlay.setBounds(125, 15, 280, 80);
		buttonPlay.addActionListener(this);
		buttonPlay.setText("PLAY");
		buttonPlay.setFocusable(false);
		icon = new ImageIcon("musical-notes.png");
		buttonPlay.setIcon(icon);
		buttonPlay.setFont(new Font("Ariel", Font.BOLD, 25));
		buttonPlay.setForeground(Color.black);
		buttonPlay.setBackground(new Color(0x5e7b9b));
		buttonPlay.setBorder(BorderFactory.createEtchedBorder());

		buttonStop = new JButton();
		buttonStop.setBounds(125, 165, 280, 80);
		buttonStop.addActionListener(this);
		buttonStop.setText("STOP");
		buttonStop.setFocusable(false);
		buttonStop.setFont(new Font("Ariel", Font.BOLD, 25));
		buttonStop.setForeground(Color.black);
		buttonStop.setBackground(new Color(0x5e7b9b));
		buttonStop.setBorder(BorderFactory.createEtchedBorder());

		this.setSize(550, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setVisible(true);
		this.add(buttonPlay);
		this.add(buttonStop);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == buttonPlay) {
			try {
				file = new File("Chicken_on_Raft.wav");
				AudioInputStream audioFile = AudioSystem.getAudioInputStream(file);
				clip = AudioSystem.getClip();
				clip.open(audioFile);
				clip.start();
			} catch (Exception e1) {
				e1.setStackTrace(null);
			}

		} else {
			clip.stop();
		}

	}
}
